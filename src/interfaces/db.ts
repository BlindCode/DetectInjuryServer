export interface User {
    name: string;
    tracking: boolean;
    danger: 'yes'|'no'|'maybe'|null;
    wait: true | false;
}

export interface Location {
    uid: string;
    lat: number;
    lng: number;
    at: number;
}

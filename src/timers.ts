export class Timers {
    private timers: {[key: string]: any} = {};

    public constructor(
        private timeout: number,
        private handler: (key: string) => void
    ) {

    }

    public start(key: string) {
        if (!this.started(key)) {
            this.timers[key] = setTimeout(() => this.handler(key), this.timeout);
        }
    }

    public restart(key: string) {
        if (this.started(key)) {
            this.stop(key);
        }

        this.timers[key] = setTimeout(() => this.handler(key), this.timeout);
    }

    public stop(key: string) {
        if (this.started(key)) {
            let timer = this.timers[key];

            clearTimeout(timer);
            delete this.timers[key];
        }
    }

    public started(key: string): boolean {
        return typeof this.timers[key] !== 'undefined';
    }
}
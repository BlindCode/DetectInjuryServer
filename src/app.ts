import * as firebase from "firebase-admin";
import { UserHandler } from "./user_handler";

let serviceAccount = require("../firebase.json");

let firebaseApp = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://awesomeprojectemotions.firebaseio.com"
});

let userHandler = new UserHandler(firebaseApp);

userHandler.attachListeners();

// firebase.auth().listUsers().then((data) => {
//     console.log("Users list:", data);
// });

